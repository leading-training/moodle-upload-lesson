<?php
$string['pluginname'] = 'Upload Lesson block';
$string['upload_lesson'] = 'Upload Lesson';
$string['upload_lesson:addinstance'] = 'Add a new lesson upload block';
$string['upload_lesson:myaddinstance'] = 'Add a new lesson upload block to the My Moodle page';
$string['view:edit_page'] = 'Edit Page';
