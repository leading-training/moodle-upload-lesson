<?php

require_once("$CFG->libdir/formslib.php");

class upload_lesson_form extends moodleform
{
    function definition()
    {

        global $CFG, $DB;

        $mform = $this->_form;

        // Display section titles
        // Select element
        $course_id = $this->_customdata['courseid'];

        $course_sections = $DB->get_records_select_menu('course_sections', "course = $course_id", null, null, 'id,name');

        $mform->addElement('select', 'section', 'Select Course Section', $course_sections);
        $mform->setType('section', PARAM_TEXT);
        $mform->addRule('section', 'Please select a course section', 'required');

        $mform->addElement('static', 'help_text', '', 'NB: if section titles do not appear above, please make sure they have been edited under the course');

        // Lesson Title
        // Select element
        $mform->addElement('text', 'title', 'Lesson Title');
        $mform->setType('title', PARAM_TEXT);
        $mform->addRule('title', 'Please provide lesson title', 'required');

        // Lesson Description
        $mform->addElement('textarea', 'description', 'Lesson Description', 'wrap="virtual" rows="5" cols="50"');
        $mform->addRule('description', 'Please provide lesson description', 'required');

        // Select element
        $mform->addElement('select', 'tag', 'Select HTML Tag', array('h1', 'h2'));
        $mform->setType('tag', PARAM_TEXT);


        // Hidden variables to store current course id and block id
        $mform->addElement('hidden', 'courseid', $this->_customdata['courseid']);
        $mform->setType('courseid', PARAM_INT);
        $mform->addElement('hidden', 'blockid', $this->_customdata['blockid']);
        $mform->setType('blockid', PARAM_INT);

        // Upload file element
        //$mform->addElement('file', 'filename', 'Upload HTML');
        $mform->addElement('filepicker', 'filename', get_string('file'), null, array('accepted_types' => '*'));
        $mform->setType('MAX_FILE_SIZE', PARAM_INT);
        $mform->addRule('filename', 'Please upload an html file', 'required');

        $this->add_action_buttons(false, 'Upload Lesson');
    }
}
