Instructions to install upload lesson block

- Required Moodle version: 3.8.3+

Installation
- Add upload_html folder to blocks directory by cloning it from the git repository using the following command: 
    git clone git@gitlab.com:leading-training/moodle-upload-lesson.git upload_lesson
- Log in to moodle as admin and follow instructions to update the database and add new plugin

Use:
- Once installed, create a new course
- Within the newly created course, turn on editing functionality
- Under 'Add a block', the new Upload HTML block will be available
- Once added to course follow the link to upload the html file that will be used to fill sections of the course

Files:

1) view.php:
    - Retrieves HTML text from uploaded file and creates lessons and displays the HTML upload form if no upload has occured

2) upload_lesson_form.php
    - creates form ui display
    - includes for elements and data type restrictions/validations

3) edit_form.php
    - provides specific definitions
    
4) block_upload_lesson.php
    - automatically generated when creating a block plugin
