<?php

require_once('../../config.php');
require_once('upload_lesson_form.php');

global $DB, $COURSE;

// Check for all required variables.
$course_id = required_param('courseid', PARAM_INT);

$block_id = required_param('blockid', PARAM_INT);

// Next look for optional variables.
$id = optional_param('id', 0, PARAM_INT);

if (!$course = $DB->get_record('course', array('id' => $course_id))) {
    print_error('Invalid course', 'upload_lesson_form', $course_id);
}



require_login($course);

$PAGE->set_url('/blocks/upload_lesson/view.php', array('id' => $id, 'courseid' => $course_id, 'blockid' => $block_id));
$PAGE->set_pagelayout('standard');
$PAGE->set_heading('Upload Lesson');

$settingsnode = $PAGE->settingsnav->add('Upload Lesson Settings');
$editurl = new moodle_url('/blocks/upload_lesson/view.php', array('id' => $id, 'courseid' => $course_id, 'blockid' => $block_id));
$editnode = $settingsnode->add('Edit Page', $editurl);
$editnode->make_active();

$htmlForm = new upload_lesson_form(null, array('courseid' => $course_id, 'blockid' => $block_id));


if ($from = $htmlForm->get_data()) {


    $file_content = $htmlForm->get_file_content('filename');
    $form_data = $htmlForm->get_submitted_data();
    $headings_array = array();
    $sections_array = array();

    $form_data->tag == 0 ? $tag = 'h1' : $tag = 'h2';


    // Will retrieve all titles within heading tag. Headings are stored 
    // in matches[1]
    preg_match_all('|<' . $tag . '[^>]*>(.*?)</' . $tag . '>|si', $file_content, $matches);

    for ($i = 0; $i < count($matches[1]); $i++) {
        array_push($headings_array, $matches[1][$i]);
    }

    // Will retrieve all segments between heading tags except the last one. Also 
    // saved in matches[1]
    preg_match_all('|</' . $tag . '>(.*?)<' . $tag . '[^>]*>|si', $file_content, $matches);
    $count = 0;
    //print_object($matches);
    foreach ($matches[1] as $section) {
        array_push($sections_array, $section);
    }

    // Will find final segment between last heading tag and end of html body
    $tag = strrev($tag);
    preg_match_all('|(.*?)>' . $tag . '/<|si', strrev($file_content), $matches);

    array_push($sections_array, strrev($matches[1][0]));

    /**
     * Create lesson instance
     */

    // Lesson object from form
    $lesson = [
        'course' => $course_id,
        'name' => $form_data->title,
        'intro' => '<p dir="ltr" style="text-align: left;">' . $form_data->description . '</p>',
        'introformat' => 1,
        'conditions' => 'O:8:"stdClass":3:{s:9:"timespent";i:0;s:9:"completed";i:0;s:15:"gradebetterthan";i:0;}',
        'grade' => 100,
        'custom' => 1,
        'maxanswers' => 5,
        'maxattempts' => 1,
        'feedback' => 0,
        'maxpages' => 1,
        'mediaheight' => 480,
        'mediawidth' => 640,
        'timemodified' => time()
    ];

    $lesson_id = $DB->insert_record('lesson', $lesson);

    /**
     * Handle Grade Item entries
     */

    // Determine if course is already in grade_items table
    $course_grade_items = $DB->get_records_select('grade_items', "courseid = $course_id AND itemtype = 'course'");
    $courses_in_grade_items = $DB->get_records_select('grade_items', "itemtype = 'course'");

    $aggregatecoef2 = number_format((float) 1, 5);

    if (empty($course_grade_items)) {
        // Make an entry for this course in grade_items and grade_categories

        $last_gc_entry = $DB->get_records('grade_categories', null, 'id desc', 'id', 0, 1);
        $next_gc_id = (int) array_key_first($last_gc_entry) + 1;

        $course_grade_category = [
            'courseid' => $course_id,
            'depth' => 1,
            'path' => '/' . $next_gc_id . '/',
            'fullname' => '?',
            'aggregation' => 13,
            'aggregateonlygraded' => 1,
            'timecreated' => time(),
            'timemodified' => time()
        ];

        $grade_category_id = $DB->insert_record('grade_categories', $course_grade_category);

        // Make an entry for this course in mdl_grade_items
        $course_grade_item = [
            'courseid' => $course_id,
            'itemtype' => 'course',
            'iteminstance' => $grade_category_id, //course instance in this table
            'timecreated' => time(),
            'timemodified' => time()
        ];

        $DB->insert_record('grade_items', $course_grade_item);
    } else {
        // Course already exists in grade table
        $count = count($course_grade_items);
        $aggregatecoef2 = number_format((float) 1 / $count + 1, 5);

        // Get grade category id 
        $grade_category = $DB->get_record_select('grade_categories', "courseid = $course_id");
        $grade_category_id = (int) $grade_category->id;
    }

    //Lesson sorting otder
    $last_sortorder = $DB->get_field_select('grade_items', 'MAX(sortorder)', "courseid = $course_id");
    if (!empty($last_sortorder)) {
        $sortorder = $last_sortorder + 1;
    } else {
        $sortorder = 1;
    }

    // grade_items entry for lesson
    $grade_item = [
        'courseid' => $course_id,
        'categoryid' => $grade_category_id,
        'itemname' => $lesson['name'],
        'itemtype' => 'mod',
        'itemmodule' => 'lesson',
        'iteminstance' => $lesson_id,
        'itemnumber' => 0,
        'aggregationcoef2' => $aggregatecoef2, //percentage depending on lessons in current course
        'timecreated' => time(),
        'timemodified' => time(),
        'sortorder' => $sortorder
    ];

    $grade_item_id = $DB->insert_record('grade_items', $grade_item);

    // Insert course module
    $module = $DB->get_record_select('modules', "name = 'lesson'");
    $module_id = (int) $module->id;

    // Insert course module table for current lesson
    $course_module = [
        'course' => $course_id,
        'module' => $module_id,
        'instance' => $lesson_id,
        'section' => $form_data->section,
        'added' => time(),
        'completion' => 1
    ];

    $course_module_id = $DB->insert_record('course_modules', $course_module);

    // Update course_sections sequence
    $current_section = $DB->get_record_select('course_sections', "id = $form_data->section");

    $course_section = [
        'id' => $current_section->id,
        'sequence' => $current_section->sequence . ',' . $course_module_id
    ];

    $DB->update_record('course_sections', $course_section);

    // Clear cache

    /**
     * Handle mdl_context table entry
     * 1. Get path for current course from the table from course's table entry
     * 2. Get max entries in table for next available id
     * 3. Append id to the path
     * 4. Increment depth by one from course path
     */

    // Course context level is 50 
    $course_context_record = $DB->get_record_select(
        'context',
        "contextlevel = 50 and instanceid = $course_id"
    );

    $last_context_entry = $DB->get_records('context', null, 'id desc', 'id', 0, 1);
    $next_context_id = (int) array_key_first($last_context_entry) + 1;

    $context = [
        'contextlevel' => 70, // Context level for module 	
        'instanceid' => $course_module_id, // Course module is the instance	
        'path' => $course_context_record->path . '/' . $next_context_id, // Path to context	
        'depth' => $course_context_record->depth + 1
    ];

    $lesson_pages = array();
    $prev_page_id = 0;

    $last_lesson_page = $DB->get_records('lesson_pages', null, 'id desc', 'id', 0, 1);
    $last_lesson_page_id = (int) array_key_first($last_lesson_page);

    for ($i = 0; $i < count($headings_array); $i++) {
        $lesson_page = [
            'lessonid' => $lesson_id,
            'prevpageid' => $prev_page_id,
            'nextpageid' => $last_lesson_page_id + 2,
            'qtype' =>    20,
            'title' =>    $headings_array[$i],
            'contents' => preg_replace('/^\s+|\r|\s+$/m', '', $sections_array[$i]),
            'contentsformat' => 1,
            'timecreated' => time(),
            'timemodified' => time()
        ];

        $prev_page_id = $last_lesson_page_id + 1;
        $last_lesson_page_id = $prev_page_id;

        array_push($lesson_pages, $lesson_page);
    }

    //update the next page id of last lesson page
    $last_page_key = array_key_last($lesson_pages);
    $lesson_pages[$last_page_key]['nextpageid'] = 0;

    // Insert into lesson_pages table
    json_encode($lesson_pages);
    $DB->insert_records('lesson_pages', $lesson_pages);

    // Clear cache

    // Redirect
    $url = new moodle_url('/course/view.php', array('id' => $course_id));
    redirect($url);
} else {

    //Display the header
    echo $OUTPUT->header();

    // Display the form
    $htmlForm->display();

    // Display the footer
    echo $OUTPUT->footer();
}
